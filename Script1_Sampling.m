
% -------------------------------------------------------------------------
% Bias Description - Script1_Sampling.m
% -------------------------------------------------------------------------
% Description
%
% This script takes noisy time series simulated with Tessier kinetics, and
% estimates the parameters of multiple models by means of Monte Carlo
% sampling.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Script1_Sampling() 
%
% INPUT
%   N/A
%
% OUTPUT
%   N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

disp('cleared')
addpath('./spike_b')
addpath('./logdet')

warning('off','MATLAB:legend:IgnoringExtraEntries')

% =======================================================================
%   INFORMATION FOR SIMULATION EXPERIMENT
% =======================================================================
rng(11) % Initialization

% Ground truth model parameters:
r_max =1;
K_S = (1:2:15)/10; % eight different values
logsigmaE = [ log(10^-2) log(10^-1)]; % two different values, model is parametrized with log(sigma_epsilon)

% Simulation parameters:
ttil = 0:1/100:8; 

% =======================================================================
%   INFORMATION FOR SAMPLING
% =======================================================================

theta_LB_all = [ 0 0 0 -12  0 0 ]';
theta_UB_all = [ +inf +inf +inf  +12 1  2 ]';
parvar0 = 10^-6 ;
nSample0=10000;
nSample1=10000;
nSample2=10000;
nSample3=20000;

% =======================================================================
%   DERIVED INFORMATION
% =======================================================================
nK = length(K_S);
nS = length(logsigmaE);
Ntil = length(ttil);
etil = randn(Ntil,1);

ttil_mat = repmat(ttil(:),[ 1 Ntil]);
dttil = abs(ttil_mat-ttil_mat'); % 

disp('setup finished')

% =======================================================================
%   EXECUTE PARAMETER SAMPLING
% =======================================================================

tic

for iK= 1:nK
    
    for iS=1:nS
        
        for xDeficit=[  false true ] %        for xDeficit=[false true ]
            
            name = ['results_step0_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  ];
            load(['./result/' name])
            
            modelsim = @(theta) fSimProcess(ttil,dSdt_model,theta);
            
            for xBias= [   false true]
                
                for xStrongRmax=[   false ]
                    clc
                    
                    
                    % ----------------------------------------------------
                    %   DEFINE LIKELIHOODS
                    % ----------------------------------------------------
                    
                    % Define feasible range for parameters
                    if ~xBias
                        theta_LB = theta_LB_all(1:end-2);
                        theta_UB = theta_UB_all(1:end-2);
                    else
                        theta_LB = theta_LB_all;
                        theta_UB = theta_UB_all;
                    end
                    
                    % Define prior and likelihood
                    hNegLogFeasible =  @(theta) -log(all(and(theta>=theta_LB,theta<=theta_UB))*1);
                    if xStrongRmax
                        hNegLogPriorRmax = @(theta) (-((theta(1)-r_max)/(.1)).^2) ;
                    else
                        hNegLogPriorRmax = @(theta) 0 ;
                    end
                    if xBias
                        hNegLogPriorTheta = @(theta) (-log(cos((theta(end)-1)/2*pi)))  ;
                        hNegLogLikelihood = @(theta) fNegLogLikelihoodBias(ytil,modelsim(theta),theta(end-2),theta(end-1),theta(end),dttil) ;
                    else
                        hNegLogPriorTheta = @(theta) 0 ;
                        hNegLogLikelihood = @(theta) fNegLogLikelihoodNoBias(ytil,modelsim(theta),theta) ;
                    end
                    
                    % Define posterior likelihood
                    hNegLogPosterior = @(theta) hNegLogFeasible(theta)+hNegLogPriorTheta(theta)+hNegLogPriorRmax(theta)+hNegLogLikelihood(theta) ;
                    
                    % ----------------------------------------------------
                    %   SAMPLING STEP 0 - NO ADAPATION - AIM TO FIND LOCAL MINIMUM
                    % ----------------------------------------------------
                    
                    name=['result_step0_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  '_'  num2str(xBias)  '_'  num2str(xStrongRmax)];
                    disp(name)
                    
                    iAdapt = +inf;
                    
                    theta_init = theta_true;
                    if xBias
                        theta_init = [ theta_init ; 1/2 ; 1 ];
                    end
                    nTheta = length(theta_init);
                    cov_theta_init = diag(ones(nTheta,1))*parvar0 ; 
                    
                    [SampleTrace0,LoglikTrace0,AcceptTrace0] = fAdaptiveMetropolisHastings(theta_init,hNegLogPosterior,nSample0,cov_theta_init,iAdapt);
                    
                    save(['./result/' name],'SampleTrace0','LoglikTrace0')
                    
                    % ----------------------------------------------------
                    %   SAMPLING STEP 1 - WITH ADAPATION - AIM TO FIND GOOD
                    %   START POINT
                    % ----------------------------------------------------
                    
                    name=['result_step1_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  '_'  num2str(xBias)  '_'  num2str(xStrongRmax)];
                    disp(name)
                    
                    iAdapt = 1000;
                    [~,iBestPosterior] = min(LoglikTrace) ;
                    theta_init = SampleTrace(iBestPosterior,:)';
                    nTheta = length(theta_init);
                    cov_theta_init = diag(ones(nTheta,1))*parvar0 ; 
                    
                    [SampleTrace1,LoglikTrace1,AcceptTrace1] = fAdaptiveMetropolisHastings(theta_init,hNegLogPosterior,nSample1,cov_theta_init,iAdapt);
                    
                    save(['./result/' name],'SampleTrace1','LoglikTrace1')
                    
                    % ----------------------------------------------------
                    %   SAMPLING STEP 2 - WITH ADAPATION - AIM TO FIND GOOD
                    %   PROPOSAL DISTRIBUTION
                    % ----------------------------------------------------
                    
                    name=['result_step2_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  '_'  num2str(xBias)  '_'  num2str(xStrongRmax)];
                    disp(name)
                    
                    iAdapt = 1000;
                    [~,iBestPosterior] = min(LoglikTrace) ;
                    theta_init = SampleTrace(iBestPosterior,:)';
                    nTheta = length(theta_init);
                    cov_theta_init = diag(ones(nTheta,1))*parvar0 ; 
                    
                    [SampleTrace2,LoglikTrace2,AcceptTrace2] = fAdaptiveMetropolisHastings(theta_init,hNegLogPosterior,nSample1,cov_theta_init,iAdapt);
                    
                    save(['./result/' name],'SampleTrace2','LoglikTrace2')
                    
                    % ----------------------------------------------------
                    %   SAMPLING STEP 3 - no ADAPATION - SAMPLE WITH
                    %   OBTAINED PROPOSAL DISTRIBUTION
                    % ----------------------------------------------------
                    
                    name=['result_step3_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  '_'  num2str(xBias)  '_'  num2str(xStrongRmax)];
                    disp(name)
                    
                    iAdapt = +inf;
                    [~,iBestPosterior] = min(LoglikTrace) ;
                    theta_init = SampleTrace(iBestPosterior,:)';
                    nTheta = length(theta_init);
                    cov_theta_init = diag(ones(nTheta,1))*parvar0 ; 
                    
                    [SampleTrace,LoglikTrace,AcceptTrace] = fAdaptiveMetropolisHastings(theta_init,hNegLogPosterior,nSample2,cov_theta_init,iAdapt);
                    
                    save(['./result/' name],'SampleTrace','LoglikTrace','AcceptTrace')
                    
                end
            end
            
        end
    end
end

time=toc ;

return