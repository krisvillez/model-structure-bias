function [SampleTrace,LoglikTrace,AcceptTrace,cov_theta] = fAdaptiveMetropolisHastings(theta_init,hNegLogPosterior,nSample,cov_theta_init,iAdapt)

% -------------------------------------------------------------------------
% Bias Description - fAdaptiveMetropolisHastings.m
% -------------------------------------------------------------------------
% Description
%
% This function executed the Metropolis-Hastings sampler with adaptation.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[SampleTrace,LoglikTrace,AcceptTrace,cov_theta] = fAdaptiveMetropolisHastings(theta_init,hNegLogPosterior,nSample,cov_theta_init,iAdapt)
%
% INPUT
%   theta_init          :   Vector of initial parameter values
%   hNegLogPosterior    :   Function representing the posterior log-likelihood
%   nSample             :   Number of samples
%   cov_theta_init      :   Initial proposal distribution covariance matrix
%   iAdapt              :   Last sample without active adaptation
%                           (iAdapt=+inf means there is no adaptation ever)
%
% OUTPUT
%   SampleTrace         :   Matrix of sampled parameter vectors 
%                           (dimensions nSample X #parameters)
%   LoglikTrace         :   Vector of evaluated posterior log-likelihood values
%   AcceptTrace         :   Vector of accept/reject decisions
%   cov_theta           :   Final proposal distribution covariance matrix
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

nTheta = length(theta_init);

%sd = 2.4^2/nTheta ;
epsilon = 1e-8 ;
Id =eye(nTheta) ;
                
SampleTrace = nan(nSample,nTheta);
AcceptTrace = false(nSample,1);
LoglikTrace = nan(nSample,1);

theta_old = theta_init;
cov_theta = cov_theta_init;
LL_old = hNegLogPosterior(theta_old);

mn_theta = theta_init ;
mn_theta_old = mn_theta;

T = nSample ;

M=chol(cov_theta);

for t=1:T
    
    if mod(t,1000)==0
        disp([num2str(floor(1000*t/T)/10,'%05.1f') ' %'])
    end
    if exist('mvnrnd','file')
        theta_delta = mvnrnd(zeros(1,nTheta), cov_theta ) ;
    else
        theta_delta = randn(1,nTheta)*M ;
    end
    theta_delta = theta_delta(:);
    theta_new = theta_old+theta_delta;
    LL_new = hNegLogPosterior(theta_new) ;
    
    if LL_new==+inf
        accept = false;
    else
        alpha = exp(LL_old-LL_new);
        if alpha>1 || and(iAdapt>=0,rand(1)<=alpha)
            accept = true;
        else
            accept = false;
        end
        
        if accept
            %theta_dev = theta_new-theta_old;
            theta_old = theta_new;
            LL_old = LL_new ;
            AcceptTrace(t)=true;
            
            %                 figure(73), hold on
            %                 plot(iSample,-LL_old,'m.')
        else
            %theta_dev = 0*(theta_new-theta_old);
        end
    end
    LoglikTrace(t)=LL_old;
    SampleTrace(t,:) = theta_old ;
    AcceptTrace(t)=accept;
    
    
    if t>iAdapt
        mn_theta = t/(t+1)*mn_theta_old + 1/(t+1)*theta_old ;
        theta_dev = theta_old(:)-mn_theta(:);
        cov_theta = t/(t+1)*cov_theta + 1/(t+1)* (	theta_dev(:)*theta_dev(:)' ...
                                                +epsilon*Id) ;
%         sd_theta = diag(cov_theta).^(1/2);
%         corrT=diag(1./sd_theta)*cov_theta*diag(1./sd_theta);
%         cov_theta = diag(max(1e-6,sd_theta))*corrT*diag(max(1e-6,sd_theta));
        mn_theta_old = mn_theta ;
        M=chol(cov_theta);
    end
    
end
