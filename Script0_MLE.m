
% -------------------------------------------------------------------------
% Bias Description - Script0_MLE.m
% -------------------------------------------------------------------------
% Description
%
% This script simulates noisy time series with Tessier kinetics, computes
% maximum likelihood estimates of the parameters of two models (Tessier and
% Monod) in the sense, displays the results, and saves the produced
% figures.  
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Script0_MLE() 
%
% INPUT
%   N/A
%
% OUTPUT
%   N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
close all
clear all

disp('cleared')
addpath('./spike_b')

warning('off','MATLAB:legend:IgnoringExtraEntries')

% =======================================================================
%   INFORMATION FOR SIMULATION EXPERIMENT
% =======================================================================
rng(11) % Initialization

% Candidate models
dSdt_Monod = @(t,S,theta)  -theta(1).*(S./abs(S+theta(2)));
dSdt_Tessier = @(t,S,theta)  -theta(1)*(1-exp(-S/theta(2)));

% Ground truth model parameters:
r_max =1;
S_0 = 5;
gamma = 0 ;
K_S = (1:2:15)/10; % eight different values
logsigmaE = [ log(10^-2) log(10^-1)]; % two different values, model is parametrized with log(sigma_epsilon)
dSdt_true = dSdt_Tessier;

% Simulation parameters:
ttil = 0:1/100:8; 

% =======================================================================
%   DERIVED INFORMATION
% =======================================================================
nK = length(K_S);
nS = length(logsigmaE);
Ntil = length(ttil);
etil = randn(Ntil,1);


disp('setup finished')

% =======================================================================
%   EXECUTE PARAMETER ESTIMATION AND DISPLAY OF RESULTS
% =======================================================================

for iK= 1:nK
    
    for iS=1:nS
        
        % ---------------------------------------------------
        %       SIMULATE EXPERIMENT 
        % ---------------------------------------------------
        theta_true = [ r_max K_S(iK) S_0 logsigmaE(iS) ]';
        
        simulation = @(theta) fSimProcess(ttil,dSdt_true,theta);
        ysim=simulation(theta_true);
        sigma = exp(theta_true(end)) ;
        ytil =ysim +etil*sigma;
        
        % Visualization
        figure(iS*10+iK), 
        subplot(2,1,1), hold on, plot(ttil,ytil,'k.','MarkerSize',11)
        subplot(2,1,2), hold on, 
        
        
        % ---------------------------------------------------
        %       ESTIMATE PARAMETERS OF TWO MODELS
        % ---------------------------------------------------
        
        for xDeficit=[  false true ] 
            if xDeficit     % Model deficit: selected wrong model structure
                dSdt_model = dSdt_Monod;
            else            % No model deficit: selected correct model structure
                dSdt_model = dSdt_true;
            end
            
            clc
            
            name = ['results_step0_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  ];
            disp(name)
            
            % Define likelihood
            loglikelihood	=	@(theta) sum((ytil-fSimProcess(ttil,dSdt_model,theta)).^2);
            
            % Optimize likelihood
            thetahat_MLE	=	fminsearch(loglikelihood,theta_true) ;
            
            % Simulate with estimated parameters
            yhat_MLE        =	fSimProcess(ttil,dSdt_model,thetahat_MLE);
            
            % Visualization
            if xDeficit
                subplot(2,1,1), plot(ttil,yhat_MLE,'r:','linewidth',2)
                if iS==nS
                    title('\sigma_e=0.1 g/m^3')
                else
                    title('\sigma_e=0.01 g/m^3')
                end
                set(gca,'Xticklabel',[])
                set(gca,'Xlim',[0 8]+1/8*[-1 +1])
                set(gca,'Ylim',[-0.2 5.2],'Ytick',[0:5])
                ylabel('$\tilde{s}(t),~\hat{s}(t)~[g/m^3]$','Interpreter','latex')
                legend({'$\tilde{s}(t)$','$\hat{s}(t)$ Tessier','$\hat{s}(t)$ Monod'},'Interpreter','latex','Location','NorthEast')
                
                subplot(2,1,2), plot(ttil,ytil-yhat_MLE,'r.','MarkerSize',7)
                set(gca,'Xlim',[0 8]+1/8*[-1 +1])
                set(gca,'Ylim',[-0.51 0.51],'Ytick',[-.5:.1:.5])
                set(gca,'Tickdir','out')
                ylabel('$\tilde{s}(t)-\hat{s}(t)~[g/m^3]$','Interpreter','latex')
                xlabel('t [h]')
                legend({'$\tilde{s}(t)-\hat{s}(t)$ Tessier','$\tilde{s}(t)-\hat{s}(t)$ Monod'},'Interpreter','latex','Location','NorthWest')
                
                SaveMyFigure(gcf,['./figure/' 'fig_MLE_iK' num2str(iK) '_iS' num2str(iS)])
            else
                subplot(2,1,1), plot(ttil,yhat_MLE,'b-','linewidth',3)
                subplot(2,1,2), plot(ttil,ytil-yhat_MLE,'b.','MarkerSize',11)
            end
            
            save(['./result/' name],'dSdt_model','theta_true','thetahat_MLE','ttil','ytil')
        end
    end
end


return