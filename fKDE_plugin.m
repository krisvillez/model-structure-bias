function pdf = fKDE_plugin(x,xi)

% -------------------------------------------------------------------------
% Bias Description - fKDE_plugin.m
% -------------------------------------------------------------------------
% Description
%
% This function implements kernel density estimation (KDE) witha plugin
% bandwidth estimator.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	pdf = fKDE_plugin(x,xi)
%
% INPUT
%   x   :   Vector of sampled values
%   xi  :   Vector of values where density should be computed
%
% OUTPUT
%   pdf	:   Vector of kernel density values
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


n= size(x,1);
x=sort(x);

IQR=fQuantile(x,3/4)-fQuantile(x,1/4) ;
stdx = std(x) ;
A = min([ IQR stdx ]);
h = A*(2/3*n)^(-1/5) ;
                    
Ni      =	length(xi)                          ;
Dist	=	repmat(x,[1 Ni])-repmat(xi,[n 1])	;
K       =	1/sqrt(2*pi)*exp(-(Dist/h).^2/2)    ;
pdf     =	sum(K,1)/(n*h)                      ;


end

