function LL = fNegLogLikelihoodBias(ytil,ysim,logsigmaE,mixB,tauB,dtsim)

% -------------------------------------------------------------------------
% Bias Description - fNegLogLikelihoodBias.m
% -------------------------------------------------------------------------
% Description
%
% This function implements the computation of posterior log-likelihood for
% models that include a bias description term.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	LL = fNegLogLikelihoodBias(ytil,ysim,logsigmaE,mixB,tauB,dtsim)
%
% INPUT
%   ytil        :   Measurements
%   ysim        :   Simulated expected measurement values
%   logsigmaE   :   Log of standard deviation parameter
%   mixB        :   Mixture parameter (alpha)
%   tauB        :   Correlation length for bias description term
%   dtsim       :   Symmetric matrix describing point-wise distances in
%                   time between pairs of measurements
%
% OUTPUT
%   LL          :   Log-likelihood
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


Ntil = length(ytil);
Nsim = length(ysim);


assert(Nsim==Ntil,'simulated data and observations vector have unequal length')
assert(all(Nsim==size(dtsim)),'provided kernel matrix does not match simulated data dimensions')

res = (ytil-ysim) ;
res =res(:);

%tauB =10.^(logtauB);
sigmaE =exp(logsigmaE);
sigmaE2 = ((1-mixB)*sigmaE^2) ;
sigmaB2 = ((mixB)*sigmaE^2) ;
% 
% sigmaB2
% sigmaE2
% tauB
if any([tauB]<=0) || or(mixB>1,mixB<0)
    LL = +inf ;
else
    K = exp(-dtsim/(Nsim*tauB)) ;
    Sigma_B = sigmaB2*K ;
    Sigma = Sigma_B +sigmaE2*eye(Nsim);
    %LL =  +1/2*sum(log(svd(Sigma))) +1/2*res'*Sigma^(-1)*res +Ntil/2*log(2*pi);
    LL =  +1/2*logdet(Sigma,'chol') +1/2*res'*Sigma^(-1)*res +Ntil/2*log(2*pi);
end

end

