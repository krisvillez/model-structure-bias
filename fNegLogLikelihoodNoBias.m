function LL = fNegLogLikelihoodNoBias(ytil,ysim,theta)

% -------------------------------------------------------------------------
% Bias Description - fNegLogLikelihoodNoBias.m
% -------------------------------------------------------------------------
% Description
%
% This function implements the computation of posterior log-likelihood for
% models that do not include a bias description term.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	LL = fNegLogLikelihoodNoBias(ytil,ysim,logsigmaE,mixB,tauB,dtsim)
%
% INPUT
%   ytil        :   Measurements
%   ysim        :   Simulated expected measurement values
%   theta       :   Vector of parameters, last one of which is used as the
%                   log of the measurement error standard deviation
%
% OUTPUT
%   LL          :   Log-likelihood
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


Ntil = length(ytil);
Nsim = length(ysim);

assert(Nsim==Ntil,'simulated data and observations vector have unequal length')

res = (ytil-ysim) ;
res =res(:);

logsigmaE = theta(end);
sigmaE2 =exp(logsigmaE)^2;

Sigma = sigmaE2*eye(Nsim);

LL =  +logsigmaE*log(10)*Nsim +1/2*res'*Sigma^(-1)*res +Ntil/2*log(2*pi);

end

