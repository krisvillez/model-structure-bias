function xq = fQuantile( x ,q)

% -------------------------------------------------------------------------
% Bias Description - fQuantile.m
% -------------------------------------------------------------------------
% Description
%
% This function computes a quantile from a set of samples.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	xq = fQuantile( x ,q)
%
% INPUT
%   q	:   fraction for which quantile is computed (0 < q < 1)
%   x	:   Sample of values
%
% OUTPUT
%   xq	:   Quantile value
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

x=sort(x);
n = length(x);
if q>0.5
    xq = x(ceil(n*q));
elseif q<0.5
    xq = x(floor(n*q));
else
    xq = (x(floor(n*q))+x(ceil(n*q)))/2;
end


end

