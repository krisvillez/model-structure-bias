function ysim	=   fSimProcess(ttil,dSdt,theta)

% -------------------------------------------------------------------------
% Bias Description - fQuantile.m
% -------------------------------------------------------------------------
% Description
%
% This function simulates an ODE model conditional to some model
% parameters.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	ysim	=   fSimProcess(ttil,dSdt,theta)
%
% INPUT
%   ttil	:   Times at which to sample the simulated states
%   dSdt	:   Function representing an ODE model in the form f(t,x,theta)
%   theta   :   Vector of parameter values
%
% OUTPUT
%   ysim	:   Simulated states matching the times in ttil
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if any(theta(1:3)<0)
    ysim = ones(length(ttil),1)*inf ;
else
    [t,y]   =	ode23tb( @(t,x) dSdt(t,x,theta(1:2)),ttil,theta(3)); %  
    ysim	=	y	; 
end

end

