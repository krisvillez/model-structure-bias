
% -------------------------------------------------------------------------
% Bias Description - Script2_Visual.m
% -------------------------------------------------------------------------
% Description
%
% This script produces the figures for the manuscript
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	Script2_Visual() 
%
% INPUT
%   N/A
%
% OUTPUT
%   N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2019-12-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019 Kris Villez
%
% This file is part of a stand-alone package. 
% 
% This package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% This package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
% close all
clear all

disp('cleared')
addpath('./spike_b')
addpath('./logdet')

warning('off','MATLAB:legend:IgnoringExtraEntries')

% =======================================================================
%   INFORMATION FOR SIMULATION EXPERIMENT
% =======================================================================
rng(11) % Initialization

% Ground truth model parameters:
r_max =1;
S_0 = 5;
gamma = 0 ;
K_S = (1:2:15)/10; % eight different values
logsigmaE = [ log(10^-2) log(10^-1)]; % two different values, model is parametrized with log(sigma_epsilon)

% Simulation parameters:
ttil = 0:1/100:8; 

% =======================================================================
%   DERIVED INFORMATION
% =======================================================================
nK = length(K_S);
nS = length(logsigmaE);

disp('setup finished')

% =======================================================================
%   EXECUTE PARAMETER SAMPLING
% =======================================================================
D=0.5;
nSampleSelect=10000;

clc
close all


for xStrongRmax=false
    
    figure(1000+xStrongRmax), hold on
    plot([0 1],[0 100],'k-','linewidth',2)


    for iS=1 %[ 1 nS]
        
        figure(100+iS*10+xStrongRmax),
        subplot(2,1,1), hold on,
        plot([0 2.5],[1 1],'k-')
        subplot(2,1,2), hold on,
        plot([0 2.5],[1 1],'k-')
        
        figure(200+iS*10+xStrongRmax),
        subplot(2,1,1), hold on,
        plot([0 2.5],[1 1],'k-')
        subplot(2,1,2), hold on,
        plot([0 2.5],[1 1],'k-')
        
        for iK=1:nK
            
            theta_true = [ r_max K_S(iK) S_0 logsigmaE(iS) ]';
            
            K_true = K_S(iK) ;
            S_true = 0.69*K_true ;
            
            for xDeficit=[ false  true ] %        for xDeficit=[false true ]
                
                %modelsim = @(theta) simprocess(ttil,dSdt_model,theta);
                
                for xBias= [  false  true]
                    
                    name=['result_step3_' num2str(iK) '_' num2str(iS) '_' num2str(xDeficit)  '_'  num2str(xBias)  '_'  num2str(xStrongRmax)];
                    
                    
                    disp(name)
                    load(['.\result\' name])
                    
                    SampleTrace = SampleTrace(end-nSampleSelect+1:end,:);
                    
                    if xDeficit
                        S_hat1 = D*SampleTrace(:,2)./(r_max-D) ;
                        S_hat2 = D*SampleTrace(:,2)./(SampleTrace(:,1)-D) ;
                        marker = '+' ;
                    else
                        S_hat1 = -SampleTrace(:,2).*log(1-D./r_max) ;
                        S_hat2 = -SampleTrace(:,2).*log(1-D./SampleTrace(:,1)) ;
                        marker = 'x' ;
                    end
                    
                    xi = [0:0.0001:5];
                    
                    x = S_hat1/S_true ;
                    F1 = fKDE_plugin(x,xi) ;
                    Q1 = [ fQuantile(x,0.005 ) fQuantile(x,0.995 ) ]';
                    
                    x = S_hat2/S_true ;
                    F2 = fKDE_plugin(x,xi) ;
                    Q2 = [ fQuantile(x,0.005 ) fQuantile(x,0.995 ) ]';
                    
                    
                    % figure A
                    figure(100+iS*10+xStrongRmax)
                    subplot(2,1,1+xDeficit*1), hold on,
                    index = F1>0;
                    x = F1(index)/max(F1(:));
                    y = xi(index) ;
                    x = [ 0 ; x(:) ; 0 ];
                    y = [ y(1) ; y(:) ; y(end) ];
                    if iS==nS
                        patch(K_true-(1-xBias*2)*[x*.05],y,'k','linewidth',0.01)
                        plot(K_true-0.05*(1-xBias*2)*(ones(2,1)*[0 1])',[Q1 Q1]','r-')
                    else
                        if xBias
                        else
                            patch(K_true-(1-xBias*2)*[x*.05],y,'k','linewidth',0.01)
                            plot(K_true-0.05*(1-xBias*2)*(ones(2,1)*[0 1])',[Q1 Q1]','r-')
                            patch(K_true+(1-xBias*2)*[x*.05],y,'k','linewidth',0.01)
                            plot(K_true+0.05*(1-xBias*2)*(ones(2,1)*[0 1])',[Q1 Q1]','r-')
                        end
                    end
                    
                    % figure B
                    figure(200+iS*10+xStrongRmax)
                    subplot(2,1,1+xDeficit*1), hold on,
                    index = F2>0;
                    plot([0 2.5],[1 1],'k-')
                    x = F2(index)/max(F2(:));
                    y = xi(index) ;
                    x = [ 0 ; x(:) ; 0 ];
                    y = [ y(1) ; y(:) ; y(end) ];
                    if iS==nS
                        patch(K_true-(1-xBias*2)*[x*.05],y,'k','linewidth',0.01)
                        plot(K_true-0.05*(1-xBias*2)*(ones(2,1)*[0 1])',[Q2 Q2]','r-')
                    else
                        if xBias
                        else
                            patch(K_true-(1-xBias*2)*[x*.05],y,'k','linewidth',0.01)
                            plot(K_true-0.05*(1-xBias*2)*(ones(2,1)*[0 1])',[Q2 Q2]','r-')
                            patch(K_true+(1-xBias*2)*[x*.05],y,'k','linewidth',0.01)
                            plot(K_true+0.05*(1-xBias*2)*(ones(2,1)*[0 1])',[Q2 Q2]','r-')
                        end
                    end
                    
                    % figure C
                    if iK==4 && iS==nS
                        if xDeficit
                            fighan = figure(2) ;
                        else
                            fighan = figure(1) ;
                        end
                        nTheta = size(SampleTrace,2);
                        
                        for p=1:nTheta
                            subplot(2,3,p), hold on,
                            if xBias
                                style = 'b-' ;
                            else
                                style = 'r--' ;
                            end
                            if p==4
                                x = log10(exp(SampleTrace(:,p)));
                            elseif p==6
                                x= SampleTrace(:,p)*ttil(end);
                            elseif p>4
                                x= SampleTrace(:,p);
                            else
                                x= SampleTrace(:,p);
                            end
                            
                            switch p
                                case 1
                                    Xlim = [ 0.95 1.25 ];
                                    Xlab = 'r_{max} [g/m^3.h]' ;
                                case 2
                                    Xlim = [ 0.4 0.8 ];
                                    Xlab = 'K_{S} [g/m^3]' ;
                                case 3
                                    Xlim = [ 4.85 5.15 ];
                                    Xlab = 's_{0} [g/m^3]' ;
                                case 4
                                    Xlim = [ -1.5 0 ];
                                    Xlab = 'log_{10}(\sigma) [-]' ;
                                case 5
                                    Xlim = [ 0  1 ];
                                    Xlab = '\alpha [-]' ;
                                case 6
                                    Xlim = [ 0 2*ttil(end) ];
                                    Xlab = '\tau [h]' ;
                            end
                            x = sort(x) ;
                            plot([min(Xlim(1),x(1)) ; x;  max(x(end),Xlim(2))],[0 [1:nSampleSelect]/nSampleSelect*100 100],style,'Linewidth',2)
                            if xBias
                                
                                set(gca,'Xlim',Xlim)
                                set(gca,'Ylim',[-1 101],'Ytick',[0:20:100],'Tickdir','out')
                                xlabel(Xlab)
                                if mod(p,3)==1
                                    ylabel('ECDF [%]')
                                end
                                if p==5
                                    PlotVertical(0,'k-','Linewidth',1.5);
                                elseif p==4
                                    PlotVertical(log10(exp(theta_true(p))),'k-','Linewidth',1.5);
                                elseif p<=4
                                    PlotVertical(theta_true(p),'k-','Linewidth',1.5);
                                end
                                if p==5
                                    plot([1:nSampleSelect]/nSampleSelect,[1:nSampleSelect]/nSampleSelect*100,'k:','Linewidth',2)
                                    set(gca,'Xtick',[0:.25:1])
                                elseif p==6
                                    plot([1:nSampleSelect]/nSampleSelect*2*ttil(end),(cos([1:nSampleSelect]/nSampleSelect*pi-pi)+1)/2*100,'k:','Linewidth',2)
                                    set(gca,'Xtick',[0:4:16])
                                end
                            end
                        end
                        
                        
                        SaveMyFigure(fighan,['./figure/fig_Parameters_Deficit' num2str(xDeficit)])
                    end
                    
                    
                    % figure D
                    if xBias
                        if iS==nS
                            figure(1000+xStrongRmax), hold on
                            alpha =sort(SampleTrace(:,5));
                            if xDeficit==true
                                plot(alpha,[1:nSampleSelect]/nSampleSelect*100,'-','Color',colorst,'linewidth',2);
                            else
                                h=plot(alpha,[1:nSampleSelect]/nSampleSelect*100,'--','linewidth',2);
                                colorst = h.Color ;
                            end
                            
                        end
                    end
                    
                    drawnow()
                    
                end
            end
        end
        
        % figure A/B
        for f=1:2
            fighan=100*f+iS*10+xStrongRmax ;
            figure(fighan)
            
            for sub=1:2
                subplot(2,1,sub), hold on,
                set(gca,'Xlim',[0 1.6],'Ylim',[0 2.1],'Xtick',[.1:.2:1.5])
                xlabel('K_S [g/m^3]')
                ylabel({'S_{true}/S_{true} [g/m^3]'})
            end
            
            
            SaveMyFigure(fighan,['./figure/fig_Predict_' num2str(iS) '_' num2str(f) '_StrongRmax' num2str(xStrongRmax) ])
        end
        
    end
    
    % figure D
    figure(1000+xStrongRmax),
    legend({'prior','K_S=0.1, Tessier','K_S=0.1, Monod',...
        'K_S=0.3, Tessier','K_S=0.3, Monod',...
        'K_S=0.5, Tessier','K_S=0.5, Monod',...
        'K_S=0.7, Tessier','K_S=0.7, Monod',...
        'K_S=0.9, Tessier','K_S=0.9, Monod',...
        'K_S=1.1, Tessier','K_S=1.1, Monod',...
        'K_S=1.3, Tessier','K_S=1.3, Monod',...
        'K_S=1.5, Tessier','K_S=1.5, Monod'},'Location','EastOutside')
    
    Xlim = [ 0  1 ];
    Xlab = '\alpha [-]' ;
    
    set(gca,'Xlim',Xlim)
    set(gca,'Ylim',[-1 101],'Ytick',[0:20:100],'Tickdir','out')
    xlabel(Xlab)
    
    ylabel('ECDF [%]')
    drawnow()
    
    SaveMyFigure(1000+xStrongRmax,['./figure/fig_Alpha_StrongRmax' num2str(xStrongRmax) ])
    
    
end

han110 = 110 ;
figure(han110)
panels = get(han110,'Children') ;

f2 = figure();
panel = subplot(1,1,1) ;
Pos = panel.Position ;
delete(panel)
ax2 = copyobj(panels(1),f2);
Pos(2) = Pos(2)+.05 ;
ax2.Position = Pos ;
SaveMyFigure(f2,['./figure/fig_Predict_' num2str(1) '_' num2str(1) '_StrongRmax' num2str(0) '_bottom'])


return