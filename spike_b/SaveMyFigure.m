function SaveMyFigure(handle,FileName)

% -------------------------------------------------------------------------
% SOAR toolbox - SaveMyFigure.m
%
% This function saves a figure in multiple format at once.
%
% Syntax: 	SaveMyFigure(handle,FileName)
%
%	Inputs: 	
%       handle      Figure handle
%       FileName    Filename for the figure without extension
%
%	Outputs: 	
%               N/A
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2017-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2013-2016 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

fig = handle;

set(fig,'Units','points');
pos = get(fig,'Position');
set(fig,'PaperPositionMode','Auto','PaperUnits','points','PaperSize',[pos(3), pos(4)])
set(gcf,'renderer','opengl')
saveas(handle,[FileName ,'.eps'], 'epsc')
saveas(handle,[FileName ,'.pdf'])