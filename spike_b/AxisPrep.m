
% -------------------------------------------------------------------------
% Spike_B toolbox - AxisPrep.m
% -------------------------------------------------------------------------
% Description
%
% This script set a number of default axis settings
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	AxisPrep() 
%
% INPUT
%   N/A
%
% OUTPUT
%   N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-17
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2006-2015 Kris Villez
%
% This file is part of the Spike_B Toolbox for Matlab/Octave. 
% 
% The Spike_B Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_B Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

set(gca,'FontSize',14)
set(gca,'FontWeight','normal')
set(gca,'LineWidth',1)
set(gca,'tickdir','out')
