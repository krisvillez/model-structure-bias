
% -------------------------------------------------------------------------
% Spike_B Toolbox v1.0
% -------------------------------------------------------------------------
%	AxisPrep		Script to set default axis properties
%	FigPrep			Script to set default figure properties
%	PlotHorizontal	Function to plot a horizontal line
%	PlotVertical	Function to plot a vertical line
%	UsingOctave		Function to evaluate wheter Octave is being used
% -------------------------------------------------------------------------
